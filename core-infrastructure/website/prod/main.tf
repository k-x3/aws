############################### Global Resources ###################################
provider "aws" {
  region = var.aws_region
}

############################### p1tn Databases ###############################

resource "aws_security_group" "ec2_database_sg" {
  name        = "${var.env}-${var.database["name"]}-sg"
  description = "Security group for ${var.env}-${var.database["name"]}-sg"
  vpc_id      = var.vpc_id

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["172.31.0.0/16"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.env
    Name        = "${var.env}-${var.database["name"]}-sg"
    Project     = "p1tn"
  }

}

module "ec2_database" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "${var.env}-${var.database["name"]}"
  instance_count         = var.database["instance_count"]

  ami                    = var.database["ami"]
  instance_type          = var.database["instance_type"]
  key_name               = var.key_name
  monitoring             = true
  vpc_security_group_ids = [aws_security_group.ec2_database_sg.id]
  subnet_id              = var.subnets[0]

  associate_public_ip_address = true

  tags = {
    Environment = var.env
    Name        = "${var.env}-${var.database["name"]}"
    Project     = "p1tn"
  }
}

#############################################################################################
# AWS Security group for AWS (RDS) p1tn WebStore PostgreSQL with custom rules
#############################################################################################

module "vote_service_sg_prod" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "user-service_${var.env}"
  description = "Security group for user-service with custom ports open within VPC, and PostgreSQL publicly open"
  vpc_id      = var.vpc_id

  ingress_cidr_blocks      = ["172.31.0.0/16"]
  ingress_rules            = ["https-443-tcp"]
  ingress_with_cidr_blocks = [
    {
      from_port   = 8080
      to_port     = 8090
      protocol    = "tcp"
      description = "User-service ports"
      cidr_blocks = "172.31.0.0/16"
    },
    {
      rule        = "postgresql-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
}



################################################################
# AWS KMS - p1tn WebStore
################################################################


data "aws_kms_secrets" "RDSp1tnWebStore" {
  secret {
    name    = "password"
    payload = "AQICAHhHarliA73zeR6e7zMQSEd7CO+p2dWuLxtTEVEuW8TbkwGRmNvvt1M6U0bhtIK3VSr2AAAAcTBvBgkqhkiG9w0BBwagYjBgAgEAMFsGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMmAkJ1zbhilv+cpPOAgEQgC7xVzqvRaGQoMw66fiR5LarWvE/4bDOdooxfJ39thzWECx6k5n4p2ftRAvu7EGz"
  }
}


###########################################################################
# AWS Relational Database Service (RDS) | PostgreSQL - p1tn WebStore
###########################################################################


module "db_postgres" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.0"

  identifier = "p1tn${var.env}"

  engine            = "postgres"
  engine_version    = "11.5"
  instance_class    = "db.t3.micro"
  allocated_storage = 50
  storage_encrypted = true

  name     = "p1tn${var.env}"
  username = "p1tn"
  password = data.aws_kms_secrets.RDSp1tnWebStore.plaintext["password"]
  port     = "5432"

  iam_database_authentication_enabled = true

  vpc_security_group_ids = [module.vote_service_sg_prod.this_security_group_id]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:15-06:00"

  multi_az = false
  
  monitoring_interval = "30"
  monitoring_role_name = "p1tn_${var.env}_MyRDSMonitoringRole"
  create_monitoring_role = true

  # disable backups to create DB faster
  backup_retention_period = 35

  tags = {
    Owner       = "user"
    Environment = var.env
    Project     = "p1tn"
  }

  enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]

  # DB subnet group
  subnet_ids = var.subnets

  # DB parameter group
  family = "default.postgres11"

  # DB option group
  major_engine_version = "11.5"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "p1tn${var.env}"

  # Database Deletion Protection
  deletion_protection = true

  parameters = [
    {
      name = "character_set_client"
      value = "utf8"
    },
    {
      name = "character_set_server"
      value = "utf8"
    }
  ]

  options = [
    {
      option_name = "MARIADB_AUDIT_PLUGIN"

      option_settings = [
        {
          name  = "SERVER_AUDIT_EVENTS"
          value = "CONNECT"
        },
        {
          name  = "SERVER_AUDIT_FILE_ROTATIONS"
          value = "37"
        }
      ]
    }
  ]
}