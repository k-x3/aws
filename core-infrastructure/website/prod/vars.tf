############################### Global Resources ###############################
variable "aws_region" {
  type = string
}

variable "env" {
  type = string
}

variable "key_name" {
  type = string
}

############################### p1tn Networking ##############################

variable "vpc_id" {
  type = string
}

variable "subnets" {
  type = list
}

############################### p1tn Databases ###############################

variable "database" {
  type = map
}