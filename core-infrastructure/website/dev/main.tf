############################### Global Resources ###################################
provider "aws" {
  region = var.aws_region
}

############################### p1tn Databases ###############################

resource "aws_security_group" "ec2_database_sg" {
  name        = "${var.env}-${var.database["name"]}-sg"
  description = "Security group for ${var.env}-${var.database["name"]}-sg"
  vpc_id      = var.vpc_id

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["172.31.0.0/16"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.env
    Name        = "${var.env}-${var.database["name"]}-sg"
    Project     = "p1tn"
  }

}

module "ec2_database" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "${var.env}-${var.database["name"]}"
  instance_count         = var.database["instance_count"]

  ami                    = var.database["ami"]
  instance_type          = var.database["instance_type"]
  key_name               = var.key_name
  monitoring             = true
  vpc_security_group_ids = [aws_security_group.ec2_database_sg.id]
  subnet_id              = var.subnets[0]

  associate_public_ip_address = true

  tags = {
    Environment = var.env
    Name        = "${var.env}-${var.database["name"]}"
    Project     = "p1tn"
  }
}

#############################################################################################
# AWS Security group for AWS (RDS) p1tn WebStore PostgreSQL with custom rules
#############################################################################################

module "vote_service_sg_dev" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "user_service_${var.env}"
  description = "Security group for user-service with custom ports open within VPC, and PostgreSQL publicly open"
  vpc_id      = var.vpc_id

  ingress_cidr_blocks      = ["172.31.0.0/16"]
  ingress_rules            = ["https-443-tcp"]
  ingress_with_cidr_blocks = [
    {
      from_port   = 8080
      to_port     = 8090
      protocol    = "tcp"
      description = "User-service ports"
      cidr_blocks = "172.31.0.0/16"
    },
    {
      rule        = "postgresql-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
}



################################################################
# AWS KMS - p1tn WebStore
################################################################


data "aws_kms_secrets" "RDSp1tnWebStore" {
  secret {
    name    = "password"
    payload = "AQICAHhHarliA73zeR6e7zMQSEd7CO+p2dWuLxtTEVEuW8TbkwGRmNvvt1M6U0bhtIK3VSr2AAAAcTBvBgkqhkiG9w0BBwagYjBgAgEAMFsGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMmAkJ1zbhilv+cpPOAgEQgC7xVzqvRaGQoMw66fiR5LarWvE/4bDOdooxfJ39thzWECx6k5n4p2ftRAvu7EGz"
  }
}


################################################################
# AWS Relational Database Service (RDS) - p1tn WebStore
################################################################


module "db_postgres" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.0"

  identifier = "p1tn${var.env}"

  engine            = "postgres"
  engine_version    = "11.5"
  instance_class    = "db.t3.micro"
  allocated_storage = 50
  storage_encrypted = true

  name     = "p1tn${var.env}"
  username = "p1tn"
  password = data.aws_kms_secrets.RDSp1tnWebStore.plaintext["password"]
  port     = "5432"

  iam_database_authentication_enabled = true

  vpc_security_group_ids = [module.vote_service_sg_dev.this_security_group_id]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:15-06:00"

  monitoring_interval = "30"
  monitoring_role_name = "p1tn_${var.env}_MyRDSMonitoringRole"
  create_monitoring_role = true

  tags = {
    Owner       = "user"
    Environment = var.env
    Project     = "p1tn"
  }

  enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]

  # DB subnet group
  subnet_ids = var.subnets

  # DB parameter group
  family = "postgres11.5"

  # DB option group
  major_engine_version = "11.5"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "p1tn${var.env}"

  # Database Deletion Protection
  deletion_protection = true

  parameters = [
    {
      name = "character_set_client"
      value = "utf8"
    },
    {
      name = "character_set_server"
      value = "utf8"
    }
  ]

  options = [
    {
      option_name = "MARIADB_AUDIT_PLUGIN"

      option_settings = [
        {
          name  = "SERVER_AUDIT_EVENTS"
          value = "CONNECT"
        },
        {
          name  = "SERVER_AUDIT_FILE_ROTATIONS"
          value = "37"
        }
      ]
    }
  ]
}


############################### p1tn Website ###############################

resource "aws_security_group" "ec2_website_sg" {
  name        = "${var.env}-${var.website["name"]}-sg"
  description = "Security group for ${var.env}-${var.website["name"]}-sg"
  vpc_id      = var.vpc_id

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["172.31.0.0/16"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.env
    Name        = "${var.env}-${var.website["name"]}-sg"
    Project     = "p1tn"
  }

}

module "ec2_website_node_1" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "${var.env}-${var.website_node_1["name"]}"
  instance_count         = var.website_node_1["instance_count"]

  ami                    = var.website_node_1["ami"]
  instance_type          = var.website_node_1["instance_type"]
  key_name               = var.key_name
  monitoring             = true
  vpc_security_group_ids = [aws_security_group.ec2_website_sg.id]
  subnet_id              = var.subnets[0]

  associate_public_ip_address = true

  tags = {
    Environment = var.env
    Name        = "${var.env}-${var.website_node_1["name"]}"
    Project     = "p1tn"
  }
}

module "ec2_website_node_2" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "${var.env}-${var.website_node_2["name"]}"
  instance_count         = var.website_node_2["instance_count"]

  ami                    = var.website_node_2["ami"]
  instance_type          = var.website_node_2["instance_type"]
  key_name               = var.key_name
  monitoring             = true
  vpc_security_group_ids = [aws_security_group.ec2_website_sg.id]
  subnet_id              = var.subnets[0]

  associate_public_ip_address = true

  tags = {
    Environment = var.env
    Name        = "${var.env}-${var.website_node_2["name"]}"
    Project     = "p1tn"
  }
}

############################### p1tn Load Balancer HAProxy ###############################

resource "aws_security_group" "ec2_haproxy_sg" {
  name        = "${var.env}-${var.haproxy["name"]}-sg"
  description = "Security group for ${var.env}-${var.haproxy["name"]}-sg"
  vpc_id      = var.vpc_id

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["172.31.0.0/16"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.env
    Name        = "${var.env}-${var.haproxy["name"]}-sg"
    Project     = "p1tn"
  }

}

module "ec2_haproxy" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "${var.env}-${var.haproxy["name"]}"
  instance_count         = var.haproxy["instance_count"]

  ami                    = var.haproxy["ami"]
  instance_type          = var.haproxy["instance_type"]
  key_name               = var.key_name
  monitoring             = true
  vpc_security_group_ids = [aws_security_group.ec2_haproxy_sg.id]
  subnet_id              = var.subnets[0]

  associate_public_ip_address = true

  tags = {
    Environment = var.env
    Name        = "${var.env}-${var.haproxy["name"]}"
    Project     = "p1tn"
  }
}

############################### p1tn ElasticSearch ###############################

resource "aws_security_group" "ec2_elasticsearch_sg" {
  name        = "${var.env}-${var.elasticsearch["name"]}-sg"
  description = "Security group for ${var.env}-${var.elasticsearch["name"]}-sg"
  vpc_id      = var.vpc_id

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["172.31.0.0/16"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.env
    Name        = "${var.env}-${var.elasticsearch["name"]}-sg"
    Project     = "p1tn"
  }

}

module "ec2_elasticsearch" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "${var.env}-${var.elasticsearch["name"]}"
  instance_count         = var.elasticsearch["instance_count"]

  ami                    = var.elasticsearch["ami"]
  instance_type          = var.elasticsearch["instance_type"]
  key_name               = var.key_name
  monitoring             = true
  vpc_security_group_ids = [aws_security_group.ec2_elasticsearch_sg.id]
  subnet_id              = var.subnets[0]

  associate_public_ip_address = true

  tags = {
    Environment = var.env
    Name        = "${var.env}-${var.elasticsearch["name"]}"
    Project     = "p1tn"
  }
}