terraform {
  backend "s3" {
    bucket = "stage-website-p1tn-core-infrastructure-state"
    key    = "terraform.tfstate"
    region = "us-east-2"
  }
}

