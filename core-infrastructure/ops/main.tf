############################### Global Resources ###################################
provider "aws" {
  region = var.aws_region
}


############################### p1tn bastion ###############################

resource "aws_security_group" "ec2_bastion_sg" {
  name        = "${var.env}-${var.bastion["name"]}-sg"
  description = "Security group for ${var.env}-${var.bastion["name"]}-sg"
  vpc_id      = var.vpc_id_p1tn

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["172.31.0.0/16"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 8080
    to_port     = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.env
    Name        = "${var.env}-${var.bastion["name"]}-sg"
    Project     = "p1tn"
  }

}

module "ec2_bastion_node_1" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "${var.env}-${var.bastion["name"]}"
  instance_count         = var.bastion["instance_count"]

  ami                    = var.bastion["ami"]
  instance_type          = var.bastion["instance_type"]
  key_name               = var.key_name
  monitoring             = true
  vpc_security_group_ids = [aws_security_group.ec2_bastion_sg.id]
  subnet_id              = var.subnets_p1tn[0]

  associate_public_ip_address = true

  tags = {
    Environment = var.env
    Name        = "${var.env}-${var.bastion["name"]}"
    Project     = "p1tn"
  }
}